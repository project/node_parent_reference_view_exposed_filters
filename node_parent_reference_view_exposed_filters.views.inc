<?php

/**
 * @file
 * Contains \Drupal\entity_reference_exposed_filters\entity_reference_exposed_filters.views.inc.
 */

/**
 * Implements hook_views_data_alter().
 */
function node_parent_reference_view_exposed_filters_views_data_alter(array &$data) {
  $data['node_field_data']['nprvef_node_titles'] = [
    'title' => t('Node Parent Reference View Exposed Filters By Parent Node Titles'),
    'filter' => [
      'title' => t('Node Parent Reference View Exposed Filters By Parent Node Titles'),
      'help' => t('Show Parent Enity Reference Node Titles as a list.'),
      'field' => 'nid',
      'id' => 'nprvef_node_titles',
    ],
  ];
}
