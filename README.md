# Entity Parent Reference View Exposed Filters

INTRODUCTION
------------
This module is design for those who are facing trouble with following scenario

If Node type(Bundle) A is using the reference of Node type(Bundle) B, but Bundle B not holding any reference of bundle A,
In this case this module provided a feature, Select filter for List of Content A Title in a view based on Bundle B (Filter Criteria => "Content Type == B").

REQUIREMENTS
------------

This module requires the following modules:
 * Latest release of Drupal 8.x.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.

CONFIGURATION
-------------
Set up a Reference Node Relationship [Content reference from entity "content_type"] in a view.
Add the "Node Parent Reference View Exposed Filters by child node title List" filter.
Set the sort options.
Set the relation and select options and save the filter.

TROUBLESHOOTING
---------------
This plugin should just work if configuration steps are followed.
It should not allow you to utilize any incompatible relationships.
If it does not please file an issue in the queue.

FAQ
---
None yet.

MAINTAINERS
-----------
https://www.drupal.org/u/chandravilas

This project has been sponsored by:
 * Srijan Technology
